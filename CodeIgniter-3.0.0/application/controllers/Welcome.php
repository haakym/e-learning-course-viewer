<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Code test controller. Gets a set of pages from a (fake) database and returns it for the user
	 * 
	 * @return	null
	 * @todo The page should implement some form of filter or display choice, and show the data in an interesting way
	 * @todo Documentation is important!
	 * @todo See the view for more instructions
	 */
	public function index()
	{
		// load helper for URL and model
		$this->load->helper('url');
		$this->load->model('page_model');

		// data to return with view
		$page_data = array(
			// get pages from model and apply any display filters that are
			// sent via $_GET request i.e. URL params: ?key=value
			'pages' => $this->page_model->filter_pages($this->input->get())
		);

		// load view with filtered data
		$this->load->view('pages_view', $page_data);
	}
}

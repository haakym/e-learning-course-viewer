<?php

/**
 * Test for models/Page_model.php
 *
 * Tests page filter methods
 */
class Page_model_test extends TestCase
{
	public function setUp()
    {
        $this->resetInstance();
        $this->CI->load->model('Page_model');
        $this->obj = $this->CI->Page_model;
    }

	/** @test */
	function can_filter_by_page_code()
	{
		// Arrange
		$target_page_code = '54e76dd07078e';

		// Act
		$result = $this->obj->filter_pages(['page_code' => $target_page_code]);

		// Assert
		// Check method only returns one "page" 
		$this->assertEquals(1, count($result));
		// Check expected page code matches
		$this->assertEquals($target_page_code, current($result)['page_code']);
	}

	/** @test */
	function can_filter_by_no_children()
	{
		// Act
		$result = $this->obj->filter_pages(['display' => 'with_no_children']);

		// Assert
		// check returned value has no children
		$this->assertEquals(0, $result['children']);
	}

	/** @test */
	function can_filter_by_badges()
	{
		// Act
		$result = $this->obj->filter_pages(['display' => 'with_badges']);

		// Assert
		foreach ($result as $page) {
			$this->assertNotNull($page['page_awards_badge']);
		}
	}

}

<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Course Overview</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
        <style>
            a.page {
                position:relative;
                z-index: 1;
                display: block;
                /*float: left;*/
                width: 300px;
            }

            .connection {
                border-width: 15px;
                border-radius: 50px;
            }

            .childPage {
                border: 5px solid green;
            }

            .otherLink {
                border: blue;
            }

            .route_max {
                background: #C8A3A3;
            }

            .route_aisha {
                background: #B6A3C8;
            }

        </style>


    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="container-fluid">

            <div class="row">
                <div class="page-header col-md-6">
                    <h1>Fraud Protection - Course Overview</h1>
                </div>
                <div class="panel panel-default col-md-3" id="filter">
                    <div class="panel-body">
                        <form method="GET">
                            <div class="form-group">
                                <label for="display"></label>
                                <select  class="form-control" name="display">
                                    <option value="">All</option>
                                    <option value="with_badges">Pages with badges</option>
                                    <option value="with_no_children">Pages with no children</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">
                                    Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="panel panel-default col-md-3" id="filter">
                    <div class="panel-body">
                        <div class="form-group">
                            <button class="btn btn-primary" id="toggleOtherLinksButton">Toggle Other Links</button>
                            
                        </div>
                    </div>
                </div>
                
            </div>



            <div id="course-overview">
                
                <?php

                    $left = 0;

                    foreach ($pages as $key => $page) {

                ?>
                    <a
                        class="page panel panel-default"
                        id="<?php echo $page['page_code']; ?>"
                        style="
                            top: <?php echo 10 * $key; ?>px;
                            left: <?php echo $left; ?>px;
                        "
                        href="?page_code=<?php echo $page['page_code']; ?>"
                    >
                        <div class="panel-heading">
                            <h3 class="panel-title"><?php echo $page['name']; ?></h3>
                        </div>
                        <?php
                            if ($page['page_awards_badge'] != '') {
                        ?>
                            <div class="panel-body 
                                
                            ">
                                <div class="badge"><?php echo $page['page_awards_badge']; ?></div>
                            </div>
                        <?php
                            }
                        ?>
                    </a>
                <?php

                        if (count($page['children']) > 1 || count($page['other_links'])) {
                            $left += 100;
                        }

                    }
                ?>

            </div>
			
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
		<!-- Latest compiled and minified Bootstrap -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		<!-- CanJS - an alternative to Angular - allows for interesting client side interactions-->
		<script src="http://canjs.com/release/2.2.4/can.jquery.js"></script>
		<!-- D3 is a brilliant library for data visualisations -->
		<script src="http://d3js.org/d3.v3.min.js"></script>
		<!-- Now over to you - have fun! -->

        <script type="text/javascript" src="http://creativecouple.github.com/jquery-timing/jquery-timing.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('js/vendor/jquery.connections.js'); ?>"></script>

        <script src="<?php echo base_url('js/plugins.js'); ?>"></script>
        <script>
        	const course = <?php echo json_encode( $pages, JSON_PRETTY_PRINT ); ?>;
        </script>
        <script src="<?php echo base_url('js/main.js'); ?>"></script>
        <script>
            
            course.forEach((page) => {
                page.children.forEach((childPage) => {
                    $('#' + page.page_code).connections({ 
                        to: '#' + childPage,
                        'class': 'connection childPage'
                    });
                });
            });

            course.forEach((page) => {
                page.other_links.forEach((otherLink) => {
                    $('#' + page.page_code).connections({
                        to: '#' + otherLink,
                        'class': 'connection otherLink'
                    });
                });
            });

            const toggleOtherLinksButton = document.getElementById('toggleOtherLinksButton');

            toggleOtherLinksButton.addEventListener('click', (event) => {
                document.querySelectorAll('.otherLink').forEach((otherLink) => {
                    otherLink.style.display == 'none' ?
                        otherLink.style.display = 'block' :
                        otherLink.style.display = 'none';
                });
            });

        </script>

    </body>
</html>

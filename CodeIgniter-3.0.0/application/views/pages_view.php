<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Course Overview</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
        <!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
		<link rel="stylesheet" href="<?php echo base_url('css/style.css'); ?>">
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="container-fluid" id="app" v-cloak>

            <div class="row">
                <h1>Fraud Protection - Course Overview</h1>
            </div>
            
            <!-- Display options with filters controlled by form and buttons -->
            <div class="panel panel-default col-md-offset-9 col-md-3" style="position: absolute; right:0; top:0; z-index: 1;">
                <div class="panel-body">
                    <h4>Display Options</h4>
                    <form method="GET">
                        <div class="form-group">
                            <label for="display"></label>
                            <select class="form-control" name="display" v-model="display">
                                <option value="">All</option>
                                <option value="with_badges">Pages with badges</option>
                                <option value="with_no_children">Pages with no children</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit">
                                Submit
                            </button>
                        </div>
                    </form>

                    <div class="form-group">
                        <!-- Switch off connections between pages that display page "Other Links" -->
                        <button
                            class="btn btn-primary"
                            id="toggleOtherLinksButton"
                        >
                            Toggle "Other Links"
                        </button>
                        <!-- Reset display filters -->
                        <a href="<?php echo base_url(); ?>" class="btn btn-primary">Clear</a>
                    </div>

                    <h4>Legend</h4>
                    <div>
                        <div class="selected" style="display: inline-block; padding: 2px;">Selected</div>
                        <div class="childPage" style="display: inline-block; padding: 2px;">Child Page</div>
                        <div class="otherLink" style="display: inline-block; padding: 2px;">Other Link</div>
                    </div>
                    <br>
                    <div>
                        <div class="route_max" style="display: inline-block; padding: 2px;">Max's Route</div>
                        <div class="route_aisha" style="display: inline-block; padding: 2px;">Aisha's Route</div>
                    </div>
                </div>
            </div>

            <div id="course-overview">
                <div
                    :class="{
                        page: true,
                        panel: true,
                        'panel-default': true,
                        route_max: page.route == 'route_max',
                        route_aisha: page.route == 'route_aisha',
                        selected: selected.page_code == page.page_code,
                        child: isSelectedChild(page.page_code),
                        otherLink: isSelectedOtherLink(page.page_code),
                    }"
                    :id="page.page_code"
                    :style="{
                        top: index * 25 + 'px',
                        left: index * 100 + 'px'
                    }"
                    v-for="(page, index) in course"
                    @click="selected = page"
                >
                    <div class="panel-body">
                        <div>
                            {{ page.name }}
                            <a :href="'?page_code=' + page['page_code']" class="btn btn-default btn-xs">
                                <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                            </a>
                        </div>
                        <div class="badge" v-if="page.page_awards_badge">
                            {{ page.page_awards_badge }}
                        </div>
                    </div>
                </div>
            </div>
			
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
		<!-- Latest compiled and minified Bootstrap -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		<!-- CanJS - an alternative to Angular - allows for interesting client side interactions-->
		<script src="http://canjs.com/release/2.2.4/can.jquery.js"></script>
		<!-- D3 is a brilliant library for data visualisations -->
		<script src="http://d3js.org/d3.v3.min.js"></script>
		<!-- Now over to you - have fun! -->

        <script type="text/javascript" src="http://creativecouple.github.com/jquery-timing/jquery-timing.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url('js/vendor/jquery.connections.js'); ?>"></script>
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

        <script src="<?php echo base_url('js/plugins.js'); ?>"></script>
        <script src="<?php echo base_url('js/main.js'); ?>"></script>
        <script>

            var app = new Vue({
                el: '#app',
                mounted:function(){
                    this.selected = this.course[0];

                    this.course.forEach((page) => {
                        page.children.forEach((childPage) => {
                            $('#' + page.page_code).connections({ 
                                to: '#' + childPage,
                                'class': 'connection childPage'
                            });
                        });
                    });

                    this.course.forEach((page) => {
                        page.other_links.forEach((otherLink) => {
                            $('#' + page.page_code).connections({
                                to: '#' + otherLink,
                                'class': 'connection otherLink'
                            });
                        });
                    });
                },
                data: {
                    course: <?php echo json_encode($pages, JSON_PRETTY_PRINT); ?>,
                    layout: {
                        left: 0
                    },
                    selected: {
                        children: [],
                        other_links: []
                    },
                    display: '<?php echo isset($_GET['display']) ? $_GET['display'] : ""; ?>'
                },
                methods: {
                    isSelectedChild: function (pageCode) {
                        return this.selected.children.includes(pageCode);
                    },
                    isSelectedOtherLink: function (pageCode) {
                        return this.selected.other_links.includes(pageCode);
                    },
                }
            });
            
            const toggleOtherLinksButton = document.getElementById('toggleOtherLinksButton');

            toggleOtherLinksButton.addEventListener('click', (event) => {
                document.querySelectorAll('.otherLink').forEach((otherLink) => {
                    otherLink.style.display == 'none' ?
                        otherLink.style.display = 'block' :
                        otherLink.style.display = 'none';
                });
            });

        </script>

    </body>
</html>
